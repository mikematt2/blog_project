const express = require("express");
const cors = require("cors");
const router = express.Router();

router.use(cors());

// GET \\

router.get("/blogs", (req, res) => {
  console.log("Getting ALL blogs");
  res.status(200).json("Success, getting all blogs.");
});

// POST \\

router.post("/blogs", (req, res) => {
  console.log("Saving new blog post.");
  // call db server function here
  if (err) {
    console.log("Error: ", err);
    return -1;
  }
  res.sendStatus(201);
});

// DELETE \\

router.delete("/blogs/:post", (req, res) => {
  const { post } = req.params;
  res.send(`Delete post ${post}`).setStatus(200);
});

// PATCH \\

router.put("/blogs/:post", (req, res) => {});

module.exports = router;
