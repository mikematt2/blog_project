const routes = require("./routes");
const express = require("express");
require("dotenv");
const morgan = require("morgan");
const app = express();
const PORT = process.env.PORT || 3000;
app.use(morgan("dev"));
app.use("/", routes);
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
